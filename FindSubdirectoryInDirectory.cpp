﻿// FindSubdirectoryInDirectory.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <filesystem>
using namespace std;
namespace fs = filesystem;

void print_menu();

int main()
{
    string directory_path = "D:\\Frontend";
    string subdirectory_name = "";
    int option = 0;
    bool is_found = false;

    while (option != 4)
    {
        print_menu();
        cout << "\nInput option: ";
        cin >> option;

        switch (option)
        {
        case 1:
            cout << "\nInput directory path (use \\\\ without \\): ";
            cin >> directory_path;
            break;
        case 2:
            cout << "\nInput subdirectory name: ";
            cin >> subdirectory_name;
            break;
        case 3:

            try {
                for (const fs::directory_entry& entry : fs::recursive_directory_iterator(directory_path))
                {
                    if (entry.is_directory()) {
                        cout << entry << '\n';
                        if (fs::absolute(entry.path()).string().find(subdirectory_name) != -1) {
                            cout << "\n-----------------------------\n"
                                << "This directory found!\n"
                                << "-----------------------------\n";
                            is_found = true;
                            break;
                        }
                    }
                }

                if (!is_found) {
                    cout << "\n-----------------------------\n"
                        << "This directory didn`t find!\n"
                        << "-----------------------------\n";
                }
                is_found = false;
            }
            catch (...) {
                cout << "\nIncorrect directory path!!!\n";
            }

            break;
        case 4:
            cout << "\nBye Bye\n";
            break;
        default:
            cout << "\nIncorrect option\n";
            break;
        }
    }

    return 0;
}

void print_menu()
{
    cout << "\n1. Enter directory path.\n"
        << "2. Enter subdirectory name.\n"
        << "3. Find subdirectory in directory.\n"
        << "4. Exit.\n";
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
